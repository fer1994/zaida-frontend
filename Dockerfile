FROM node:jessie AS builder
RUN npm install -g @angular/cli@7.3.8
WORKDIR /zaida
ADD ./package.json /zaida
RUN npm install
ADD . /zaida
RUN ng build --prod


FROM nginx:alpine

ADD ./nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /zaida/dist/ZaidaFront /usr/share/nginx/html 
#ADD ./dist/ZaidaFront /usr/share/nginx/html
