import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './components/login/login.component';
import { AdminProcessComponent } from './components/admin-process/admin-process.component';
import { AdminStaffComponent } from './components/admin-staff/admin-staff.component';
import { AdminStationComponent } from './components/admin-station/admin-station.component';
import { AdminStatusComponent } from './components/admin-status/admin-status.component';
import { ConfigurationPanelComponent } from './components/configuration-panel/configuration-panel.component';
import { ReportsComponent } from './components/reports/reports.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ChartsModule } from 'ng2-charts';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { TurnComponent } from './components/turn/turn.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CreationTurnComponent } from './components/creation-turn/creation-turn.component';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatChipsModule } from '@angular/material/chips';
import { MatSelectModule} from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import localeEsAr from '@angular/common/locales/es-AR';
import { registerLocaleData } from '@angular/common';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Interceptor } from './guard/interceptor.guard';
import { FooterComponent } from './components/shared/footer/footer.component';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { SocketService } from './service/socket.service';
import { NgxGaugeModule } from 'ngx-gauge';
import { ActivityPanelComponent } from './components/activity-panel/activity-panel.component';
import { AdminProductComponent } from './components/admin-product/admin-product.component';
import { EmployeeModule } from './workspace/employee/employee.module';
import { SharedModule } from './shared/shared.module';


registerLocaleData(localeEsAr, 'es-Ar');
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminProcessComponent,
    AdminStaffComponent,
    AdminStationComponent,
    ConfigurationPanelComponent,
    ReportsComponent,
    WelcomeComponent,
    TurnComponent,
    CreationTurnComponent,
    NavbarComponent,
    FooterComponent,
    AdminStatusComponent,
    ActivityPanelComponent,
    AdminProductComponent,
  ],
  imports: [
    EmployeeModule,
    SharedModule,
    NgxGaugeModule,
    BrowserModule,
    ChartsModule,
    HttpClientModule,
    [RouterModule.forRoot(ROUTES, {onSameUrlNavigation: 'reload'})],
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TabsModule.forRoot(),
    NgxPaginationModule,
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    /** MATERIAL */
    MatInputModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatRadioModule,
    MatChipsModule,
    MatSelectModule,
    MatIconModule,
    MatAutocompleteModule,
    MatDialogModule
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
    {provide: LOCALE_ID, useValue: 'es-Ar'},
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    SocketService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
