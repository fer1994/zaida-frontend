import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(
    private http: HttpClient,
    ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers;
    const token = localStorage.getItem('token');
    if (token) {
      headers = new HttpHeaders().set('Authorization', 'Bearer ' +  token);
    } else {
      headers = new HttpHeaders();
    }

    const AuthRequest = request.clone( { headers: headers });

    return next.handle(AuthRequest);

  }
}
