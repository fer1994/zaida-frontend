export interface Workstation {
    boxNumber?: number;
    adress?: string;
    switchEnable?: boolean;
  }
