import { Workstation } from './workstation.interface';
export interface Employee {
    _id?: string;
    name?: string;
    lastName?: string;
    email?: string;
    admin?: boolean;
    password?: string;
    workStation?: Workstation;
  }
