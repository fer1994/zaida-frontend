export interface Client {
    _id?: string;
    name?: string;
    lastName?: string;
    phone?: string;
    dni?: number;
    email?: string;
    origin?: string;
  }
