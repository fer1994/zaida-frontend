export interface Product {
    _id?: string;
    name?: string;
    description?: string;
    categories?: any[];
}
