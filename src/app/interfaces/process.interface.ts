export interface Process {
    _id?: string;
    name?: string;
    description?: string;
    suggestions?: any[];
    isSelected?: boolean;
}
