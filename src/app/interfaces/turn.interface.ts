import { Process } from './process.interface';
import { Client } from './client.interface';
import { Employee } from './employees.interface';
import { Product } from './product.interface';

export interface Turn {
    _id?: string;
    process?: Process;
    notes?: string[];
    state?: string;
    client?: Client;
    employee?: Employee;
    time?: number;
    origin?: string;
    pendingActions?: string;
    priority?: number;
    code?: string;
    group?: string;
    products?: Product[];
  }
