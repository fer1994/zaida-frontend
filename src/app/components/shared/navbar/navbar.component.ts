import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../../service/login.service';
import { SettingsService } from '../../../service/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() isLogged: boolean;

  constructor(private router: Router,
              private loginService: LoginService,
              private activeRoute: ActivatedRoute,
              private settingService: SettingsService) {}

  ngOnInit() {
    this.loginService.logged.subscribe(logged => this.isLogged = logged);
  }

  async cerrarSesion() {
    let employee = JSON.parse(localStorage.getItem('user'));
    let loginTime: any = new Date(localStorage.getItem('loginTime'));
    let logoutTime: any = new Date();
    employee.session_time = (logoutTime - loginTime) / 1000.0;
    await this.loginService.logout(employee).subscribe( data => {
      localStorage.clear();

      this.settingService.setNewStation('');
      this.settingService.setUserName(null);
      this.router.navigate(['/login']);
    });
  }

}
