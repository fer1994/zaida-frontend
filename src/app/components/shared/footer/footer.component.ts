import { Component, OnInit, Input } from '@angular/core';
import { SocketService } from '../../../service/socket.service';
import { SettingsService } from '../../../service/settings.service';
import { LoginService } from '../../../service/login.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  fechaHora: Date = new Date();
  backendName: string = null;
  stationName: string = null;
  connected: boolean = false;
  username: string = 'Anonimo';
  @Input() isLogged;

  constructor(private settingsService: SettingsService,
              private loginService: LoginService,
              private socketService: SocketService) { }

  startDate() {
    setInterval(() => {
      this.fechaHora = new Date();
    }, 1000);
  }

  ngOnInit() {

    this.settingsService.user.subscribe(data => {
      if (data) {
        this.username = data.email.split("@")[0];
        this.socketService.sendNickname(this.username);
        this.socketService.sendEmail(data.email);
        this.connected = true;
      } else {
        this.loginService.logged.subscribe(logged => {
          if ( logged ) {
            this.username = JSON.parse(localStorage.getItem('user')).email.split("@")[0];
            this.connected = true;
          } else {
            this.isLogged = false;
            this.username = 'Anonimo';
            this.connected = false;
          }
        });
      }
    });

    this.settingsService.station.subscribe(station => {
        this.socketService.sendStation(station);
        this.stationName = station;
    });
    this.settingsService.connected.subscribe(connected => this.connected = connected);
    this.getSettings();
  }

  getSettings() {
    this.settingsService.getSettings();
  }
}
