import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  this.startTimer();

  }

  seconds: number = 0;
  minuts: number = 0;
  interval;

startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds < 60) {
        this.seconds++;
      }else{
        this.seconds = 0;
        this.minuts++;
      }
      
    },1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

}
