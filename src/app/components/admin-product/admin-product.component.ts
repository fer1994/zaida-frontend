import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { ErrorStateMatcher } from '@angular/material/core';
import { Component, ElementRef, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AdminService } from 'src/app/service/admin.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Product } from 'src/app/interfaces/product.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && isSubmitted);
  }
}

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.css']
})
export class AdminProductComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalService: BsModalService
  ) {}

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  pageActual: number = 1;
  addOnBlur: boolean = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];


  clearInput(event: MatChipInputEvent): void {
    const input = event.input;
    if (input) {
      input.value = '';
    }
  }

  addCategory(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.value;
    if ((value || '').trim()) {
      const validated_category = this.products.find(x => x.name == value.trim())
      if (validated_category) {
        this.categories.push(validated_category);
      }
    }
    this.adminProductForm.controls['categories'].setValue(this.categories);
  }

  removeCategory(product: any): void {
    const index = this.categories.indexOf(product);
    if (index >= 0) {
      this.categories.splice(index, 1);
    }
    this.adminProductForm.controls['categories'].setValue(this.categories);
  }

  addCategoryEdit(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.value;;
    if ((value || '').trim()) {
      const validated_category = this.products.find(x => x.name == value.trim())
      if (validated_category) {
        this.productToEdit.categories.push(validated_category);
      }
    }
    this.adminProductFormEdit.controls['categories'].setValue(this.productToEdit.categories);
  }

  removeCategoryEdit(product: any): void {
    const index = this.productToEdit.categories.indexOf(product);

    if (index >= 0) {
      this.productToEdit.categories.splice(index, 1);
    }
    this.adminProductFormEdit.controls['categories'].setValue(this.productToEdit.categories);
  }


  ngOnInit() {
    this.buildForm();
    this.getProducts();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  @ViewChild('templateProductMessage', { static: true }) templateProductMessage;
  @ViewChild('templateEdit', { static: true }) templateEdit;
  matcher = new MyErrorStateMatcher();
  modalRef: BsModalRef;
  idProductToEdit: '';
  msgModal: string = 'Producto agregado con éxito!';
  adminProductForm: FormGroup;
  adminProductFormEdit: FormGroup;
  submit: boolean = false;
  submitEdit: boolean = false;
  productToEdit: any;
  products: any = [];
  filteredProducts: any = [];
  productsCopy: any = [];
  categories: any = [];
  search: string = '';

  get f1() { return this.adminProductForm.controls; }
  get f2() { return this.adminProductFormEdit.controls; }

  buildForm() {
    this.adminProductForm = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      categories: [''],
      filteredProducts: [''],
    })
  }

  buildFormEdit() {
    this.adminProductFormEdit = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      categories: [''],
    })
  }

  addProduct() {
    this.submit = true
    if (this.adminProductForm.valid) {
      // New product
      this.adminService.createProduct(this.adminProductForm.value).subscribe(data => {
        this.getProducts();
        this.submit = false;
        this.buildForm();
        this.msgModal = 'Producto agregado con éxito!'
        this.openModal(this.templateProductMessage);
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
      this.cleanInputs();
    }
  }

  updateProduct() {
    this.submitEdit = true;

    const productToEdit = {
      name: '',
      description: '',
      categories: []
    }
    if (this.adminProductFormEdit.valid) {

      this.adminService.updateProduct(this.idProductToEdit, this.adminProductFormEdit.value).subscribe(data => {
        this.msgModal = 'Producto modificado con éxito!';
        this.modalRef.hide()
        this.openModal(this.templateProductMessage);
        this.getProducts();
        this.submitEdit = false;
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    }

  }

  getProducts() {
    this.adminService.getProducts().subscribe(data => {
      this.products = data.products;
      this.filteredProducts = data.products;
      this.productsCopy = data.products;
    });
  }

  editProduct(idProductToEdit) {
    this.idProductToEdit = idProductToEdit;
    this.buildFormEdit();
    this.adminService.getProductById(idProductToEdit).subscribe(data => {
      this.msgModal = 'Producto modificado con éxito!';
      this.openModal(this.templateEdit);
      this.productToEdit = data.product;
      console.log('data tienesdasdaeee: ', this.productToEdit.categories )
      this.productToEdit.categories = this.productToEdit.categories.map(product => this.products.find(p => p._id == product._id))
      if (this.productToEdit.categories[0] === undefined) { this.productToEdit.categories = [] }
      this.adminProductFormEdit.controls['name'].setValue(this.productToEdit.name);
      this.adminProductFormEdit.controls['description'].setValue(this.productToEdit.description);
      this.adminProductFormEdit.controls['categories'].setValue(this.productToEdit.categories);
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }

  deleteProduct(idProductToDelete) {
    this.adminService.deleteProduct(idProductToDelete).subscribe(data => {
      this.getProducts();
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }

  searchProduct(searchText) {
    if (searchText.value === '') {
      this.products = this.productsCopy;
    } else {
      this.products = this.productsCopy.filter(p => {
        return (p.name.toLowerCase().includes(searchText.value.toLowerCase()) || p.description.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }

  filterProduct(searchText) {
    if (searchText.value === '') {
      this.filteredProducts = this.productsCopy;
    } else {
      this.filteredProducts = this.productsCopy.filter(p => {
        return (p.name.toLowerCase().includes(searchText.value.toLowerCase()) || p.description.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }

  cleanInputs() {
    this.adminProductForm.controls.name.setValue('');
    this.adminProductForm.controls.description.setValue('');
    this.adminProductForm.controls.categories.setValue([]);
    this.adminProductForm.controls.filteredProducts.setValue([]);
    this.categories = [];
    this.filteredProducts = [];
  }

  openPopup(text: string) {
    this.msgModal = text;
    this.modalRef = this.modalService.show(this.templateProductMessage);
  }

}
