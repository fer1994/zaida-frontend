import { Component, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AdminService } from 'src/app/service/admin.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && isSubmitted);
  }
}


@Component({
  selector: 'app-admin-staff',
  templateUrl: './admin-staff.component.html',
  styleUrls: ['./admin-staff.component.css']
})
export class AdminStaffComponent implements OnInit {

  @ViewChild('templateStaffMessage', { static: true }) templateStaffMessage;
  @ViewChild('templateEdit', { static: true }) templateEdit;
  isGlobalAdmin: boolean = false;
  isLocalAdmin: boolean = false;
  userCredentials: any;
  localAdress: string = ''
  matcher = new MyErrorStateMatcher();
  modalRef: BsModalRef;
  msgModal: string = 'Integrante agregado con éxito!';
  idStaffMemberToEdit: '';
  adminStaffForm: FormGroup;
  adminStaffFormEdit: FormGroup;
  submit: boolean = false;
  pageActual: number = 1;
  submitEdit: boolean = false;
  adminStaffToSend = {
    name: 'inicial',
    lastName: '',
    email: '',
    password: '',
    admin: false,
    workStation: {
      adress: ''
    }
  };
  staffMembers: any = [];
  staffMembersCopy: any = [];
  workStations: any = [];
  workStationsWithOutDuplicates: [];
  workStationsByAdress: string[] = [];
  puestosEnUso: string[];
  staffMemberToEdit: any;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalService: BsModalService,
    private router: Router) { }

  ngOnInit() {
    const userCredentials = JSON.parse(localStorage.getItem("user"));
    this.userCredentials = userCredentials;
    if (userCredentials != null) {
      this.adminService.getStaffMemberById(userCredentials._id).subscribe(data => {
        if (data.employees.workStation.boxNumber === '' && data.employees.workStation.adress === '') {
          // ADMIN GLOBAL DE TODAS LAS SUCURSALES
          this.isGlobalAdmin = true;
          this.isLocalAdmin = false;
          this.localAdress = '';
        } else {
          // ADMIN LOCAL DE UNA SUCURSAL EN PARTICULAR
          this.localAdress = this.userCredentials.workStation.adress;
          this.isGlobalAdmin = false;
          this.isLocalAdmin = true;
        }
        this.getStaffMembers(this.userCredentials);
        this.getWorkStations(this.userCredentials);
      });
    } else {
      // NO ES NINGUN TIPO DE ADMIN
      this.router.navigate(['/login']);
    }

    if (this.isGlobalAdmin) {
      this.buildForm();
    } else {
      this.buildFormLocalAdmin();
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  get f1() { return this.adminStaffForm.controls; }
  get f2() { return this.adminStaffFormEdit.controls; }

  buildFormLocalAdmin() {
    this.adminStaffForm = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      adress: [''],
      admin: [false]
    });
  }

  buildForm() {
    this.adminStaffForm = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      adress: ['', [Validators.required]],
      admin: [false]
    });
  }

  buildFormEdit() {
    this.adminStaffFormEdit = this.fb.group({
      nameEdit: ['', [Validators.required]],
      lastNameEdit: ['', [Validators.required]],
      emailEdit: ['', [Validators.required, Validators.email]],
      adressEdit: ['', [Validators.required]],
      adminEdit: [false]
    });
  }

  addStaffMember() {
    this.submit = true;
    if (this.isGlobalAdmin) {
      // Es un administrador Global
      if (this.adminStaffForm.valid) {
        this.adminStaffToSend.name = this.adminStaffForm.controls.name.value;
        this.adminStaffToSend.lastName = this.adminStaffForm.controls.lastName.value;
        this.adminStaffToSend.email = this.adminStaffForm.controls.email.value;
        this.adminStaffToSend.workStation.adress = this.adminStaffForm.controls.adress.value;
        this.adminStaffToSend.password = this.adminStaffForm.controls.password.value;
        this.adminStaffToSend.admin = this.adminStaffForm.controls.admin.value;
        // New staffMember
        console.log('llega a hacer la peticion')
        this.adminService.createStaffMember(this.adminStaffToSend).subscribe(data => {
          console.log('data que vuelve del addStaff: ', data)
          if (data.status === 409) {
            // Significa que tienen el mismo email
            this.msgModal = 'El integrante que intentas agregar ya existe!'
          } else {
            // Significa que tienen distinto email
            console.log('token: ', data.token)
            this.getStaffMembers(this.userCredentials);
            this.msgModal = 'Integrante agregado con éxito!'
          }
          this.submit = false;
          this.cleanInputs();
          this.openModal(this.templateStaffMessage);
        },
          err => {
            this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
            console.log(err);
          });
      }
    } else {
      // Es un administrador Local
      if (this.adminStaffForm.valid) {
        this.adminStaffToSend.name = this.adminStaffForm.controls.name.value;
        this.adminStaffToSend.lastName = this.adminStaffForm.controls.lastName.value;
        this.adminStaffToSend.email = this.adminStaffForm.controls.email.value;
        this.adminStaffToSend.workStation.adress = this.localAdress;
        this.adminStaffToSend.password = this.adminStaffForm.controls.password.value;
        this.adminStaffToSend.admin = this.adminStaffForm.controls.admin.value;
        // New staffMember
        this.adminService.createStaffMember(this.adminStaffToSend).subscribe(data => {
          if (data.status === 409) {
            this.msgModal = 'El integrante que intentas agregar ya existe!'
          } else {
            this.getStaffMembers(this.userCredentials);
            this.msgModal = 'Integrante agregado con éxito!'
          }
          this.submit = false;
          this.cleanInputs();
          this.openModal(this.templateStaffMessage);
        },
          err => {
            this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
            console.log(err);
          });
      }
    }
  }

  updateStaffService() {
    this.submitEdit = true;

    const staffMemberEdit = {
      name: '',
      lastName: '',
      adress: '',
      email: '',
      admin: false
    };

    if (this.adminStaffFormEdit.valid) {
      staffMemberEdit.name = this.adminStaffFormEdit.controls['nameEdit'].value;
      staffMemberEdit.lastName = this.adminStaffFormEdit.controls['lastNameEdit'].value;
      staffMemberEdit.adress = this.adminStaffFormEdit.controls['adressEdit'].value;
      staffMemberEdit.email = this.adminStaffFormEdit.controls['emailEdit'].value;
      staffMemberEdit.admin = this.adminStaffFormEdit.controls['adminEdit'].value;
      this.adminService.updateStaffMember(this.idStaffMemberToEdit, staffMemberEdit).subscribe(data => {
        this.msgModal = 'Integrante modificado con éxito!';
        this.modalRef.hide()
        this.openModal(this.templateStaffMessage);
        this.getStaffMembers(this.userCredentials);
        this.submitEdit = false;
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err);
        });
    }
  }

  getStaffMembers(userCredentials) {
    if (this.isGlobalAdmin) {
      // Obtengo todos
      this.adminService.getStaffMembers().subscribe(data => {
        this.staffMembers = data.employees;
        this.staffMembersCopy = data.employees;
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        })
    } else {
      // Obtengo los que corresponden a la direccion del admin local
      this.adminService.getStaffMembersByUserCredentials(userCredentials.workStation.adress).subscribe(data => {
        this.staffMembers = data.employees;
        this.staffMembersCopy = data.employees;
      },
        err => { console.log(err) })
    }
  }

  getWorkStations(userCredentials) {
    if (this.isGlobalAdmin) {
      // Obtengo todas
      this.adminService.getWorkStations().subscribe(data => {
        this.workStations = data.workStations;
        this.workStationsWithOutDuplicates = this.removeDuplicates(data.workStations, 'adress')
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    } else {
      // Obtengo las que corresponden a la direccion del admin local
      this.adminService.getWorkStationsByUserCredentials(userCredentials.workStation.adress).subscribe(data => {
        this.workStations = data.workStations;
        this.workStationsWithOutDuplicates = this.removeDuplicates(data.workStations, 'adress')
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    }
  }

  removeDuplicates(arr, comp) {
    const unique = arr
      .map(e => e[comp])
      .map((e, i, final) => final.indexOf(e) === i && i)
      .filter(e => arr[e]).map(e => arr[e]);
    return unique;
  }

  editStaffMember(idStaffMemberToEdit) {
    this.idStaffMemberToEdit = idStaffMemberToEdit;
    this.buildFormEdit();
    this.openModal(this.templateEdit);
    this.adminService.getStaffMemberById(idStaffMemberToEdit).subscribe(data => {
      this.staffMemberToEdit = data.employees;
      this.adminStaffFormEdit.controls['nameEdit'].setValue(this.staffMemberToEdit.name);
      this.adminStaffFormEdit.controls['lastNameEdit'].setValue(this.staffMemberToEdit.lastName);
      this.adminStaffFormEdit.controls['emailEdit'].setValue(this.staffMemberToEdit.email);
      this.adminStaffFormEdit.controls['adressEdit'].setValue(this.staffMemberToEdit.workStation.adress);
      // La linea de abajo se agrega para poner en duro el localAdress, en caso de que sea un admin quien edita un staff:
      this.localAdress = this.staffMemberToEdit.workStation.adress;
      if (this.staffMemberToEdit.admin) {
        this.adminStaffFormEdit.controls['adminEdit'].setValue(true);
      } else {
        this.adminStaffFormEdit.controls['adminEdit'].setValue(false);
      }
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }
  deleteStaffMember(idStaffMemberToDelete) {
    this.adminService.deleteStaffMember(idStaffMemberToDelete).subscribe(data => {
      this.getStaffMembers(this.userCredentials);
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      })
  }

  searchStaff(searchText) {
    if (searchText.value === '') {
      this.staffMembers = this.staffMembersCopy;
    } else {
      this.staffMembers = this.staffMembersCopy.filter(w => {
        return ((w.name + " " + w.lastName).toLowerCase().includes(searchText.value.toLowerCase()) || w.workStation.adress.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }


  cleanInputs() {
    this.adminStaffForm.controls.name.setValue('');
    this.adminStaffForm.controls.lastName.setValue('');
    this.adminStaffForm.controls.email.setValue('');
    this.adminStaffForm.controls.password.setValue('');
    this.adminStaffForm.controls.adress.setValue('');
    this.adminStaffForm.controls.admin.setValue(false);
  }

  openPopup(text: string) {
    this.msgModal = text;
    this.modalRef = this.modalService.show(this.templateStaffMessage);
  }

}
