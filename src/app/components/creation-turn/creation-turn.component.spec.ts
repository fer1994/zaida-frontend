import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationTurnComponent } from './creation-turn.component';

describe('CreationTurnComponent', () => {
  let component: CreationTurnComponent;
  let fixture: ComponentFixture<CreationTurnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationTurnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationTurnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
