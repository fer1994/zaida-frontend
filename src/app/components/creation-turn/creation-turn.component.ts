import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Turn } from '../../interfaces/turn.interface';
import { Client } from '../../interfaces/client.interface';
import { Employee } from '../../interfaces/employees.interface';
import { TurnService } from '../../service/turn.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Process } from '../../interfaces/process.interface';
import { ActivatedRoute, Params, Router } from '@angular/router';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-creation-turn',
  templateUrl: './creation-turn.component.html',
  styleUrls: ['./creation-turn.component.css']
})
export class CreationTurnComponent implements OnInit {

  formTurn: FormGroup;
  matcher = new MyErrorStateMatcher();
  submit = false;
  turn: Turn = {};
  @ViewChild('templateError', { static: true }) modalTemplate;
  modalText: string;
  modalRef: BsModalRef;
  processList: Process[] = [];
  userLogged: Employee = {};
  fastTurn: boolean = false;

  constructor(
    private fb: FormBuilder,
    private turnService: TurnService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getProcess();
    this.buildForm();
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    this.validateFastTurn();
  }


  validateFastTurn() {

    const fastTurnAux = this.route.snapshot.queryParamMap.get('fastTurn');
    if (fastTurnAux) {
      if (fastTurnAux === 'true') {
        this.fastTurn = true;
      } else {
        this.fastTurn = false;
      }
    } else {
      this.fastTurn = false;
    }

    const idTurn = this.route.snapshot.queryParamMap.get('idTurn');
    if (idTurn) {
      this.turnService.getTurnById(idTurn).subscribe( response => {
        if (response) {
          const turnAux: Turn = response;
          this.formTurn = this.fb.group({
            name: [turnAux.client.name],
            lastName: [turnAux.client.lastName],
            nroLinea: [turnAux.client.phone],
            dni: [turnAux.client.dni],
            email: [turnAux.client.email],
            notes: [turnAux.notes],
            process: [''],
            origin: ['Walkin']
          });
          this.fastTurn = true;
        }

      }, error => {
        console.log(error);
      });
    }


  }

  get f1() { return this.formTurn.controls; }

  getProcess() {
    this.turnService.getAllProcess().subscribe( data => {
      this.processList = data.process;
    },
    error => {
      this.openPopupError('Error al obtener los procesos.');
    });

  }

  buildForm() {
    this.formTurn = this.fb.group({
      name: [''],
      lastName: [''],
      nroLinea: [''],
      dni: [''],
      email: [''],
      notes: [''],
      process: [''],
      origin: ['Walkin']
    });
  }

  openPopupError(text: string) {
    this.modalText = text;
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  onSubmit() {
    if (this.formTurn.valid) {
      const client: Client = {};
      client.name = this.formTurn.controls.name.value;
      client.lastName = this.formTurn.controls.lastName.value;
      client.email = this.formTurn.controls.email.value;
      client.phone = this.formTurn.controls.nroLinea.value;
      client.dni = this.formTurn.controls.dni.value;

      this.turn.client = client;
      this.turn.priority = 1;
      this.turn.process = this.formTurn.controls.process.value;
      this.turn.notes = this.formTurn.controls.notes.value;
      this.turn.origin = this.formTurn.controls.origin.value;
      if (this.fastTurn) {
        // TODO replace false with a variable enabled 
        // only if you are in a quick turn creation  
        this.turn.employee = this.userLogged;
        this.turn.state = 'opened';
      }

      if(localStorage.getItem('station')) {
        this.turn.group = localStorage.getItem('station')
      }else{
        this.openPopupError('Debe seleccionar una estacion');
        return;
      }

      if (this.turn.process === '') {
        this.openPopupError('Debe seleccionar un proceso');
        return;
      }

      //METODO PARA CREAR TURNO SIN ASIGNAR
      this.turnService.createTurn(this.turn).subscribe( data => {
        this.buildForm();
        for(let process of this.processList) {
          process.isSelected = false;
        }

        if (this.fastTurn) {
          this.router.navigate(['/turn']);
        } else {
          console.log(data);
          this.openPopupError('El turno se registro correctamente');
        }
      },
      error => {
        console.log(error);
        this.openPopupError('Error al conectarse con el servidor');
      });
    } else {
      this.openPopupError('Complete los datos requeridos');
    }

  }

  onChangeChip(process: Process){
      this.processList.map(function (dato) {
        if (dato._id === process._id) {
          dato.isSelected = true;
        } else {
          dato.isSelected = false;
        }
        return dato;
      });
      this.f1.process.setValue(process);
    }

}
