import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AdminService } from '../../service/admin.service';
import { Employee } from '../../interfaces/employees.interface';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoginService } from '../../service/login.service';
import { SettingsService } from '../../service/settings.service';
import { IdleSessionTimeout } from "idle-session-timeout";


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  //time out in 30 min on inactivity
  session = new IdleSessionTimeout(90 * 60 * 1000);
  hide = true;
  loginForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  submit: boolean = false;
  employee: Employee;
  @ViewChild('template', { static: true }) modalTemplate;
  modalText: string;
  modalRef: BsModalRef;
  usuarioPass: boolean = false;

  constructor(private fb: FormBuilder,
    private adminService: LoginService,
    private modalService: BsModalService,
    private settingService: SettingsService,
    private router: Router) { }

  ngOnInit() {
    this.buildForm();
    this.validateUserLogged();

    this.session.onTimeOut = () => {
      // here you can call your server to log out the user
          this.cerrarSesion();
          this.openPopup('Tu sesión ha expirado.');
      };

  }

  async cerrarSesion() {
    let employee = JSON.parse(localStorage.getItem('user'));
    let loginTime: any = new Date(localStorage.getItem('loginTime'))
    let logoutTime: any = new Date()
    employee.session_time = (logoutTime - loginTime)/ 1000.0
    await this.adminService.logout(employee).subscribe( data => {
      localStorage.clear();
      this.router.navigate(['/login']);
    })
  }

  validateUserLogged() {
    const user = localStorage.getItem('user');
    if (user !== null) {
      this.router.navigate(['/welcome']);
    }
  }

  get f1() { return this.loginForm.controls; }

  buildForm() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', Validators.required],
    });

  }

  onSubmit() {
    this.submit = true;
    if (this.loginForm.valid) {

      const username = this.loginForm.controls.username.value;
      const password = this.loginForm.controls.password.value;
      this.adminService.getStaffByUsernameAndPass(username, password).subscribe(data => {
        if (data.status === 200) {
          localStorage.setItem('user', JSON.stringify(data.user));
          localStorage.setItem('token', data.token);
          this.adminService.loginSuccessful(true);

          if (data.user.workStation.adress) {
            localStorage.setItem('station', data.user.workStation.adress);
            this.settingService.setNewStation(localStorage.getItem('station'));
          } else {
            this.settingService.getAllStation().subscribe(response => {
              if (response.status === 200) {
                if (response.stationAdresses.length > 0) {
                  localStorage.setItem('station', response.stationAdresses[0]);
                  this.settingService.setNewStation(localStorage.getItem('station'));
                }
              }
            });
          }
          this.session.start();
          this.settingService.setUserName(JSON.parse(localStorage.getItem('user')));
          this.router.navigate(['/welcome']);
        } else {
          this.openPopup('Usuario o contraseña incorrecto.');
          this.usuarioPass = true;
          this.adminService.loginSuccessful(false);
          this.buildForm();
        }

      },
        err => {
          console.log(err);
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          this.adminService.loginSuccessful(false);
        });
    }
  }

  openPopup(text: string) {
    this.modalText = text;
    this.modalRef = this.modalService.show(this.modalTemplate);
  }
  

}
