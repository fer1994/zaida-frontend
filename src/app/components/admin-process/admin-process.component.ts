import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { ErrorStateMatcher } from '@angular/material/core';
import { Component, ElementRef, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AdminService } from 'src/app/service/admin.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Process } from 'src/app/interfaces/process.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && isSubmitted);
  }
}

@Component({
  selector: 'app-admin-process',
  templateUrl: './admin-process.component.html',
  styleUrls: ['./admin-process.component.css']
})
export class AdminProcessComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalService: BsModalService
  ) {}

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  pageActual: number = 1;
  addOnBlur: boolean = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];


  clearInput(event: MatChipInputEvent): void {
    const input = event.input;
    if (input) {
      input.value = '';
    }
  }

  addSuggestion(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.value;
    if ((value || '').trim()) {
      const validated_suggestion = this.processes.find(x => x.name == value.trim())
      if (validated_suggestion) {
        this.suggestions.push(validated_suggestion);
      }
    }
    this.adminProcessForm.controls['suggestions'].setValue(this.suggestions);
  }

  removeSuggestion(process: any): void {
    const index = this.suggestions.indexOf(process);
    if (index >= 0) {
      this.suggestions.splice(index, 1);
    }
    this.adminProcessForm.controls['suggestions'].setValue(this.suggestions);
  }

  addSuggestionEdit(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.value;;
    if ((value || '').trim()) {
      const validated_suggestion = this.processes.find(x => x.name == value.trim())
      if (validated_suggestion) {
        this.processToEdit.suggestions.push(validated_suggestion);
      }
    }
    this.adminProcessFormEdit.controls['suggestions'].setValue(this.processToEdit.suggestions);
  }

  removeSuggestionEdit(process: any): void {
    const index = this.processToEdit.suggestions.indexOf(process);

    if (index >= 0) {
      this.processToEdit.suggestions.splice(index, 1);
    }
    this.adminProcessFormEdit.controls['suggestions'].setValue(this.processToEdit.suggestions);
  }


  ngOnInit() {
    this.buildForm();
    this.getProcesses();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  @ViewChild('templateProcessMessage', { static: true }) templateProcessMessage;
  @ViewChild('templateEdit', { static: true }) templateEdit;
  matcher = new MyErrorStateMatcher();
  modalRef: BsModalRef;
  idProcessToEdit: '';
  msgModal: string = 'Proceso agregado con éxito!';
  adminProcessForm: FormGroup;
  adminProcessFormEdit: FormGroup;
  submit: boolean = false;
  submitEdit: boolean = false;
  processToEdit: any;
  processes: any = [];
  filteredProcesses: any = [];
  processesCopy: any = [];
  suggestions: any = [];
  search: string = '';

  get f1() { return this.adminProcessForm.controls; }
  get f2() { return this.adminProcessFormEdit.controls; }

  buildForm() {
    this.adminProcessForm = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      suggestions: [''],
      filteredProcesses: [''],
    })
  }

  buildFormEdit() {
    this.adminProcessFormEdit = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      suggestions: [''],
    })
  }

  addProcess() {
    this.submit = true
    if (this.adminProcessForm.valid) {
      // New process
      this.adminService.createProcess(this.adminProcessForm.value).subscribe(data => {
        this.getProcesses();
        this.submit = false;
        this.buildForm();
        this.msgModal = 'Proceso agregado con éxito!'
        this.openModal(this.templateProcessMessage);
      },
        err => {
          console.log('llegue')
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
      this.cleanInputs();
    }
  }

  updateProcess() {
    this.submitEdit = true;

    const processToEdit = {
      name: '',
      description: '',
      suggestions: []
    }
    if (this.adminProcessFormEdit.valid) {

      this.adminService.updateProcess(this.idProcessToEdit, this.adminProcessFormEdit.value).subscribe(data => {
        this.msgModal = 'Proceso modificado con éxito!';
        this.modalRef.hide()
        this.openModal(this.templateProcessMessage);
        this.getProcesses();
        this.submitEdit = false;
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    }

  }

  getProcesses() {
    this.adminService.getProcesses().subscribe(data => {
      this.processes = data.process;
      this.filteredProcesses = data.process;
      this.processesCopy = data.process;
    });
  }

  editProcess(idProcessToEdit) {
    this.idProcessToEdit = idProcessToEdit;
    this.buildFormEdit();
    this.adminService.getProcessById(idProcessToEdit).subscribe(data => {
      this.msgModal = 'Proceso modificado con éxito!';
      this.openModal(this.templateEdit);
      this.processToEdit = data.process;
      this.processToEdit.suggestions = this.processToEdit.suggestions.map(s => this.processes.find(p => p._id == s))
      if (this.processToEdit.suggestions[0] === undefined) { this.processToEdit.suggestions = [] }
      this.adminProcessFormEdit.controls['name'].setValue(this.processToEdit.name);
      this.adminProcessFormEdit.controls['description'].setValue(this.processToEdit.description);
      this.adminProcessFormEdit.controls['suggestions'].setValue(this.processToEdit.suggestions);
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }

  deleteProcess(idProcessToDelete) {
    this.adminService.deleteProcess(idProcessToDelete).subscribe(data => {
      this.getProcesses();
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }

  searchProcess(searchText) {
    if (searchText.value === '') {
      this.processes = this.processesCopy;
    } else {
      this.processes = this.processesCopy.filter(p => {
        return (p.name.toLowerCase().includes(searchText.value.toLowerCase()) || p.description.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }

  filterProcess(searchText) {
    if (searchText.value === '') {
      this.filteredProcesses = this.processesCopy;
    } else {
      this.filteredProcesses = this.processesCopy.filter(p => {
        return (p.name.toLowerCase().includes(searchText.value.toLowerCase()) || p.description.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }

  cleanInputs() {
    this.adminProcessForm.controls.name.setValue('');
    this.adminProcessForm.controls.description.setValue('');
    this.adminProcessForm.controls.suggestions.setValue([]);
    this.adminProcessForm.controls.filteredProcesses.setValue([]);
    this.suggestions = [];
    this.filteredProcesses = [];
  }

  openPopup(text: string) {
    console.log('se ejecuta openpopup')
    this.msgModal = text;
    this.modalRef = this.modalService.show(this.templateProcessMessage);
  }
}
