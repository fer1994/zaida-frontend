import { Component, OnInit } from '@angular/core';
import { TurnService } from '../../service/turn.service';
import { Employee } from '../../interfaces/employees.interface';


@Component({
  selector: 'app-activity-panel',
  templateUrl: './activity-panel.component.html',
  styleUrls: ['./activity-panel.component.css']
})
export class ActivityPanelComponent implements OnInit {

  turns: any = [];
  filteredTurns: any = [];
  page: number = 1;
  userLogged: Employee = {};

  constructor(private turnService: TurnService) {}

  ngOnInit() {
  	  this.userLogged = JSON.parse(localStorage.getItem('user'));
	  this.turnService.getClosedTurns(this.userLogged._id).subscribe(data => {
	      if (data.status === 200) {
	        this.turns = data.turn;
	        this.filteredTurns = data.turn;
	      }
	    }, error => {
	      console.log(error);
	    })
  }

  search(searchText) {
    console.log('this.filteredTurn',this.filteredTurns)
    if (searchText.value === '') {
      this.filteredTurns = this.turns;
    } else {
      this.filteredTurns = this.turns.filter(w => {
        return (w.createdAt && w.createdAt.includes(searchText.value) || w.client.name && w.client.name.toLowerCase().includes(searchText.value.toLowerCase()) || w.client.lastName && w.client.lastName.toLowerCase().includes(searchText.value.toLowerCase()) || w.client.phone && w.client.phone.toString().includes(searchText.value) || w.process && w.process.name.toLowerCase().includes(searchText.value.toLowerCase()) || w.pendingActions && w.pendingActions.toLowerCase().includes(searchText.value.toLowerCase())
         || w.notes && w.notes.some( note => note.toLowerCase().includes(searchText.value)) );
      })
    }
  }

}
