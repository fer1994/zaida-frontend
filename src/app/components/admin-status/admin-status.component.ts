import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../service/socket.service';

@Component({
  selector: 'app-admin-status',
  templateUrl: './admin-status.component.html',
  styleUrls: ['./admin-status.component.css'],
})
export class AdminStatusComponent implements OnInit {
  clients: any = [];
  filteredClients: any = [];
  onlineUsers: string = "0";
  freeUsers: string  = "100";
  busyUsers: string = "0";
  page: number = 1;

  constructor(private socketService: SocketService) { }

  ngOnInit() {
      this.socketService.updateClients().subscribe(data => {
      	this.onlineUsers = data.length.toString();
      	this.clients = data;
      	this.filteredClients = data;
		var busyUsers = (this.clients.filter( c => c.status === "busy").length / this.clients.length)
		var freeUsers = 1 - busyUsers
      	this.freeUsers = (freeUsers * 100.0).toString();
      	this.busyUsers = (busyUsers * 100.0).toString();
      });
  }

  search(searchText) {
    if (searchText.value === '') {
      this.filteredClients = this.clients;
    } else {
      this.filteredClients = this.clients.filter(w => {
        return (w.station && w.station.toLowerCase().includes(searchText.value.toLowerCase()) || w.email && w.email.toLowerCase().includes(searchText.value.toLowerCase()) || w.status && w.status.toLowerCase().includes(searchText.value.toLowerCase()));
      })
    }
  }

}
