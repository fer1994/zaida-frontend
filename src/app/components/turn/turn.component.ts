import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { TurnService } from '../../service/turn.service';
import { Turn } from '../../interfaces/turn.interface';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ErrorStateMatcher } from '@angular/material/core';
import { Process } from '../../interfaces/process.interface';
import { Employee } from '../../interfaces/employees.interface';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login.service';
import { SocketService } from '../../service/socket.service';
import { MatDialog } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-turn',
  templateUrl: './turn.component.html',
  styleUrls: ['./turn.component.css']
})
export class TurnComponent implements OnInit, OnDestroy {
  seconds: number = 0;
  process: any = {};
  interval;
  turnForm: FormGroup;
  submit: boolean = false;
  turnList: Turn[] = [];

  userLogged: Employee = {};

  @ViewChild('templateError', { static: true }) modalTemplate;
  @ViewChild('templatePausa', { static: true }) modalTemplatePausa;
  @ViewChild('templateFinish', { static: true }) modalTemplateFinish;
  @ViewChild('templateAcciones', { static: true }) modalTemplateAccion;
  @ViewChild('templateSugerencias', { static: true }) modalTemplateSugerencias;

  station: string;
  modalText: string;
  matcher = new MyErrorStateMatcher();
  modalRef: BsModalRef;
  turnSelected: Turn;
  processList: Process[] = [];
  motivoSeleted = '';
  motivoString = '';

  constructor(
    private fb: FormBuilder,
    private turnService: TurnService,
    private modalService: BsModalService,
    private router: Router,
    private loginService: LoginService,
    private socketService: SocketService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loginService.deleteCerrarSesion();
    this.station = localStorage.getItem('station');
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    this.buildForm();
    this.getMyTurns();
    this.socketService.sendStatus('busy');
  }

  ngOnDestroy(): void {
    this.loginService.addCerrarSesion();
  }

  get f1() { return this.turnForm.controls; }

  buildForm() {
    this.turnForm = this.fb.group({
      notes: [''],
      time: this.seconds,
      turn: ['', Validators.required],
      name: [''],
      lastName: [''],
      numberline: [''],
      dni: [''],
      email: [''],
      origin: ['']
    });
  }

  async llamarCliente() {
    this.modalRef.hide();
    let newTurn: Turn = {}
    await this.turnService.getNextTurn(this.userLogged._id, this.station).then(data => {
      if (data.status === 200) {
        newTurn = data.turn;
      } else {
        this.openPopupError('No hay turnos pedientes por tomar.');
      }
    }).catch(error => {
      this.openPopupError('Error al obtener un nuevo turno');
    });

    if (newTurn._id) {
      this.turnList.push(newTurn);
      this.selectClient(newTurn);
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.seconds++;
    }, 1000);
  }

  restartTimer() {
    this.seconds = 0;
  }

  getMyTurns() {
    this.turnService.getOpenedTurns(this.userLogged._id).subscribe(data => {
      if (data.status === 200) {
        this.turnList = data.turn;
      }
      if (this.turnList.length > 0) {
        this.selectClient(this.turnList[this.turnList.length - 1]);
      }
    }, error => {
      console.log(error);
    })
  }

  async getNewTurn() {
    await this.turnService.getNextTurn(this.userLogged._id, this.station).then(data => {
      if (data.status === 204) {
        this.openPopupError('No hay turnos en la cola de turnos');
      } else {
        this.turnList.push(data.turn);
      }
    }).catch(error => {
      console.log('error ', JSON.parse(error));
    });
  }

  stopInterval() {
    clearInterval(this.interval);
  }

  fastTurn() {
    this.router.navigate(['/turn-creation'], { queryParams: { fastTurn: true } });
  }

  saveTurn(): boolean {
    let valid: boolean = true;
    if (this.turnForm.valid) {
      if (this.turnSelected) {
        this.turnSelected.group = localStorage.getItem('station');
        const turnTime = this.seconds;
        const beforeTurn = this.turnList.find(x => x._id === this.turnSelected._id);
        this.turnList.map(function (dato) {
          if (dato._id === beforeTurn._id) {
            dato = beforeTurn;
            dato.time = turnTime;
          }
          return dato;
        });

        this.turnSelected.employee = this.userLogged;
        this.turnSelected.client.name = this.turnForm.controls.name.value;
        this.turnSelected.client.lastName = this.turnForm.controls.lastName.value;
        this.turnSelected.client.email = this.turnForm.controls.email.value;
        this.turnSelected.client.phone = this.turnForm.controls.numberline.value;
        this.turnSelected.client.dni = this.turnForm.controls.dni.value;
        this.turnSelected.notes = this.turnForm.controls.notes.value;
        this.turnSelected.origin = this.turnForm.controls.origin.value;
        this.turnSelected.priority = 1;
        this.turnSelected.state = 'opened';

        this.stopInterval();
        this.seconds = 0;
        this.turnService.updateTurn(this.turnSelected).subscribe(data => {
          this.turnSelected = null;
          this.processList = [];
        },
          error => {
            console.log(error);
            this.openPopupError('Error al pausar el turno');
            valid = false;
          });

      } else {
        valid = false;
      }
    } else {
      valid = false;
    }
    return valid;
  }

  pauseTurn() {
    if (this.saveTurn()) {
      this.turnForm.patchValue({
        notes: '',
        time: '',
        turn: [''],
        name: '',
        lastName: '',
        numberline: '',
        dni: '',
        email: '',
        origin: ''
      });

      this.stopInterval();
      this.seconds = 0;
      this.turnService.updateTurn(this.turnSelected).subscribe(data => {
        this.turnSelected = null;
        this.processList = [];
      },
        error => {
          console.log(error);
          this.openPopupError('Error al pausar el turno');
        });
      this.modalRef = this.modalService.show(this.modalTemplatePausa);
    }
  }

  selectClient(turn: Turn) {
    this.stopInterval();
    this.startTimer();
    if (this.turnSelected) {
      const beforeTurn = this.turnList.find(x => x._id === this.turnSelected._id);
      const turnTime = this.seconds;
      const group = localStorage.getItem('station');
      const nombre = this.turnForm.controls.name.value;
      const apellido = this.turnForm.controls.lastName.value;
      const phone = this.turnForm.controls.numberline.value;
      const email = this.turnForm.controls.email.value;
      const dni = this.turnForm.controls.dni.value;
      const notes = this.turnForm.controls.notes.value;
      const origin = this.turnForm.controls.origin.value;

      this.turnList.map(function (dato) {
        if (dato._id === beforeTurn._id) {
          dato.time = turnTime;
          dato.client.name = nombre;
          dato.client.lastName = apellido;
          dato.client.phone = phone;
          dato.client.email = email;
          dato.origin = origin;
          dato.client.dni = dni;
          dato.notes = notes;
          dato.priority = 1;
          dato.group = group;

        }
        return dato;
      });
    }
    this.process = turn.process;
    if (turn.time) {
      this.seconds = turn.time;
    } else {
      this.restartTimer();
    }
    this.turnSelected = turn;
    this.processList = [];
    this.obtaingProcessSuggest(turn.process);
    this.turnForm.patchValue({
      notes: turn.notes,
      time: this.seconds,
      turn: [''],
      name: turn.client.name,
      lastName: turn.client.lastName,
      numberline: turn.client.phone,
      dni: turn.client.dni,
      email: turn.client.email,
      origin: turn.origin
    });
  }

  obtaingProcessSuggest(process: Process) {
    if (process && process.suggestions && process.suggestions[0] !== "") {
      for (const idProcess of process.suggestions) {
        this.turnService.getProcessId(idProcess).subscribe(data => {
          this.processList.push(data.process);
        },
          error => {
            console.log('Error', error);
          });
      }
    }
  }

  freeTurn() {
    if (this.turnSelected) {

      const turn = this.turnSelected;
      this.turnSelected.employee = null;
      this.turnSelected.state = null;
      this.turnService.updateTurn(this.turnSelected).subscribe(data => {
        const turnoListAux = this.turnList.filter(ele => {
          return ele !== turn;
        });
        this.turnList = turnoListAux;
        this.turnSelected = null;
        this.stopInterval();
        this.seconds = 0;
        this.process = '';
        this.processList = [];
      },
        error => {
          console.log(error);
          this.openPopupError('Error al derivar el turno');
        });
    } else {
      this.openPopupError('No hay turnos seleccionados');
    }
  }

  getAllProcess() {
    this.turnService.getAllProcess().subscribe(data => {

      for (let process of data.process) {
        process.isSelected = false;
      }

      this.processList = data.process;
    },
      error => {
        this.openPopupError('Error al obtener los procesos.');
      })
  }

  openPopupError(text: string) {
    this.modalText = text;
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  openPopupFinish(bandera: boolean) {
    if (this.turnForm.valid) {
      if (bandera) {
        this.modalRef.hide();
      }
      this.modalRef = this.modalService.show(this.modalTemplateFinish);
      this.stopInterval();
    }
  }

  onChangeChip(process: Process) {
    process.isSelected = !process.isSelected;
    this.processList.map(function (dato) {
      if (dato._id === process._id) {
        dato.isSelected = process.isSelected;
      }
      return dato;
    });
  }

  finishTurn(pendingAction: boolean, pendingActionString = '') {
    this.modalRef.hide();
    const turn = this.turnSelected;
    this.turnSelected.employee = this.userLogged;
    this.turnSelected.group = localStorage.getItem('station');
    this.turnSelected.state = 'closed';
    this.turnSelected.client.name = this.turnForm.controls.name.value;
    this.turnSelected.client.lastName = this.turnForm.controls.lastName.value;
    this.turnSelected.client.email = this.turnForm.controls.email.value;
    this.turnSelected.client.phone = this.turnForm.controls.numberline.value;
    this.turnSelected.client.dni = this.turnForm.controls.dni.value;
    this.turnSelected.origin = this.turnForm.controls.origin.value;
    this.turnSelected.notes = this.turnForm.controls.notes.value;
    this.turnSelected.time = this.seconds;
    this.turnSelected.priority = 1;

    if (pendingAction) {
      this.turnSelected.pendingActions = pendingActionString;
    }

    this.stopInterval();

    this.turnService.updateTurn(this.turnSelected).subscribe(data => {
      const turnoListAux = this.turnList.filter(ele => {
        return ele !== turn;
      });
      this.turnList = turnoListAux;
      this.turnSelected = null;
      this.processList = [];
      if (this.turnList.length !== 0) {
        this.selectClient(this.turnList[0]);
      }

      this.openPopupError('Turno finalizado correctamente');
    },
      error => {
        console.log(error);
        this.openPopupError('Error al finalizar el turno');
      });

    this.modalRef.hide();

  }

  openPopAcciones() {
    this.modalRef.hide();
    this.modalRef = this.modalService.show(this.modalTemplateAccion);
  }

  enviarAccion(pendingAction) {
    this.modalRef.hide();
    if (null !== pendingAction && pendingAction !== '') {
      console.log(this.motivoSeleted);
      console.log(this.motivoString);
      this.finishTurn(true, this.motivoSeleted);
    } else {
      this.openPopupError('Debe escribir una accion para continuar');
    }

  }

  enviarFinalizar() {
    if (this.turnForm.valid) {
      let bandera = false;
      if (this.processList.length > 0) {
        for (let process of this.processList) {
          if (!process.isSelected) {
            bandera = true;
          }
        }
      }

      if(!this.turnSelected.process){
        this.openPopupError('Seleccione un processo para finalizar.');
        return;
      } 

      if (this.turnSelected.process.suggestions[0] === '') {
        bandera = false;
      }

      if (!bandera) {
        this.openPopupFinish(false);
      } else {
        this.modalRef = this.modalService.show(this.modalTemplateSugerencias);
      }
    }
  }

  onVolver() {
    for (let turnAux of this.turnList) {
      this.turnService.updateTurn(turnAux).subscribe(data => { }, error => {
        console.log(error);
      });
    }
    this.socketService.sendStatus('');
    this.router.navigate(['/login']);
  }

  cloneTurn() {
    if (this.saveTurn()) {
      const turn: Turn = this.turnSelected;
      this.router.navigate(['/turn-creation'], { queryParams: { idTurn: turn._id } });
    }
  }

  changeMotivo( motivo ) {
    if (motivo === 'Cliente no cumple requisitos'){
      this.motivoString = 'Especificar que requisito no cumplia';
    } else if ( motivo === 'Producto sin stock') {
      this.motivoString = 'Especificar si es equipo o accesorio y el modelo';
    } else if ( motivo === 'Problema durante la atención') {
      this.motivoString = 'Especificar el problema';
    } else {
      this.motivoString = '';
    }
  }

  changeMotivoString(motivoString) {
    this.motivoString = motivoString;
  }
}
