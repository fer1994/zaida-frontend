import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, BaseChartDirective, Color } from 'ng2-charts';
import { Employee } from 'src/app/interfaces/employees.interface';
import { SettingsService } from '../../service/settings.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminService } from '../../service/admin.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  user: Employee = {};
  hide = true;
  hideRepeat = true;
  fechaHora: Date = new Date();
  efemeride: String = '';
  stations: String[] = [];
  stationAssigned: string;
  modalText: string;
  modalRef: BsModalRef;
  passForm: FormGroup;
  password = '';
  @ViewChild('templateSucces', { static: true }) templateSucces;
  repeatpassword = '';
  @ViewChild('template', { static: true }) modalTemplate;
  showError: boolean = false;
  showErrorText: string;

  constructor(private settingService: SettingsService,
              private modalService: BsModalService,
              private adminService: AdminService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.buildFormpass();
    this.stationAssigned = localStorage.getItem('station');
    this.user = JSON.parse(localStorage.getItem('user'));
    this.validateUser();
    this.getAllStation();
  }

  getAllStation() {

    this.settingService.getAllStation().subscribe( response => {
      if (response.status === 200) {
        this.stations = response.stationAdresses;
      }
    }, error => {
      console.log(error);
    });
  }

  changeStation(station) {
    this.settingService.setNewStation(station)
  }

  validateUser() {
    const user:Employee = JSON.parse(localStorage.getItem('user'));
  }

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];

  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];


  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  @ViewChild(BaseChartDirective, { static: false }) chart: BaseChartDirective;

  public randomize(): void {
    for (let i = 0; i < this.lineChartData.length; i++) {
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        this.lineChartData[i].data[j] = this.generateNumber(i);
      }
    }
    this.chart.update();
  }

  private generateNumber(i: number) {
    return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public hideOne() {
    const isHidden = this.chart.isDatasetHidden(1);
    this.chart.hideDataset(1, !isHidden);
  }

  public pushOne() {
    this.lineChartData.forEach((x, i) => {
      const num = this.generateNumber(i);
      const data: number[] = x.data as number[];
      data.push(num);
    });
    this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
  }

  public changeColor() {
    this.lineChartColors[2].borderColor = 'green';
    this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
  }

  public changeLabel() {
    this.lineChartLabels[2] = ['1st Line', '2nd Line'];
    // this.chart.update();
  }

  openPopup() {
    this.modalRef = this.modalService.show(this.modalTemplate);
  }

  get f1() { return this.passForm.controls; }

  cambiarClave() {

    const pass = this.passForm.controls.password.value;
    const repeatPass = this.passForm.controls.repeatPassword.value;

    if (pass !== '' && repeatPass !== '') {
      if ( pass !== '' && pass === repeatPass ) {
        console.log('ingreso bien');
        this.user.password = pass;
        this.adminService.updateStaffMember(this.user._id, this.user).subscribe( data => {
          console.log('CAMBIO CLAVE', data);
          this.modalRef.hide();
          if (data.status === 200) {
            this.buildFormpass();
            this.showError = false;
            this.modalText = 'La contraseña cambio exitosamente';
            this.openPopupSucces();
          }
        });
      } else {
        this.showError = true;
        this.showErrorText = 'Las contraseñas no coinciden';
        this.buildFormpass();
      }
    }  else  {
      this.showErrorText = 'Complete los campos vacios';
      this.showError = true;
  }

  }

  openPopupSucces() {
    this.modalRef = this.modalService.show(this.templateSucces);
  }

  buildFormpass() {
    this.passForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(2)]],
      repeatPassword: ['', Validators.required],
    });

  }

}
