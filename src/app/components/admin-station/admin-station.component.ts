import { Component, TemplateRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AdminService } from 'src/app/service/admin.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { Workstation } from '../../interfaces/workstation.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && isSubmitted);
  }
}

@Component({
  selector: 'app-admin-station',
  templateUrl: './admin-station.component.html',
  styleUrls: ['./admin-station.component.css']
})

export class AdminStationComponent implements OnInit {

  @ViewChild('templateStationMessage', { static: true }) templateStationMessage;
  @ViewChild('templateEdit', { static: true }) templateEdit;
  isGlobalAdmin: boolean = false;
  idWorkStationToEdit: '';
  matcher = new MyErrorStateMatcher();
  modalRef: BsModalRef;
  msgModal: string = 'Estación agregada con éxito!';
  adminStationForm: FormGroup;
  adminStationFormEdit: FormGroup;
  submit: boolean = false;
  submitEdit: boolean = false;
  workStations: any[];
  workStationsByLocalAdmin: any[];
  workStationsWithOutDuplicates: [];
  copyWorkStation: any[];
  multipleBoxes: boolean = false;
  pageActual: number = 1;
  switchNewStation: boolean = false;
  workStationToEdit: any;
  msgEstaciones: boolean = true;
  userCredentials: any;
  localAdress: string = ''
  adressToEdit: string = ''

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private modalService: BsModalService,
    private router: Router
  ) { }

  ngOnInit() {

    //this.getWorkStations();
    if (this.isGlobalAdmin) {
      this.buildFormOneOne();
    } else {
      this.buildFormLocalAdminOne();
    }


    let userCredentials = JSON.parse(localStorage.getItem("user"));
    this.userCredentials = userCredentials;
    if (userCredentials != null) {
      // ES UN TIPO DE ADMIN
      this.adminService.getStaffMemberById(userCredentials._id).subscribe(data => {
        if (data.employees.workStation.boxNumber === '' && data.employees.workStation.adress === '') {
          // ADMIN GLOBAL DE TODAS LAS SUCURSALES
          this.isGlobalAdmin = true;
          this.localAdress = '';
        } else {
          // ADMIN LOCAL DE UNA SUCURSAL EN PARTICULAR
          this.localAdress = this.userCredentials.workStation.adress;
          this.isGlobalAdmin = false;
        }
        this.getWorkStations(this.userCredentials);
      })
    } else {
      // NO ES NINGUN TIPO DE ADMIN
      this.router.navigate(['/login']);
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  get f1() { return this.adminStationForm.controls; }
  get f2() { return this.adminStationFormEdit.controls; }

  buildFormLocalAdminOne() {
    this.adminStationForm = this.fb.group({
      boxNumber: ['', Validators.required],
      firstBox: [''],
      lastBox: [''],
      adress: [''],
      switchEnable: [true],
    })
  }

  buildFormLocalAdminMultiples() {
    this.adminStationForm = this.fb.group({
      boxNumber: [''],
      firstBox: ['', Validators.required],
      lastBox: ['', Validators.required],
      adress: [''],
      switchEnable: [true],
    })
  }

  buildFormOneOne() {
    this.adminStationForm = this.fb.group({
      boxNumber: ['', Validators.required],
      firstBox: [''],
      lastBox: [''],
      adress: ['', Validators.required],
      switchEnable: [true],
    })
  }

  buildFormOneTwo() {
    this.adminStationForm = this.fb.group({
      boxNumber: [''],
      firstBox: ['', Validators.required],
      lastBox: ['', Validators.required],
      adress: ['', Validators.required],
      switchEnable: [true],
    })
  }

  buildFormEdit() {
    this.adminStationFormEdit = this.fb.group({
      boxNumberFormEdit: ['', Validators.required],
      adressFormEdit: ['', Validators.required],
      switchEnableFormEdit: [true],
    })
  }

  addStation() {
    this.submit = true;
    let workStationsCountToAdd;

    if (this.isGlobalAdmin) {
      // Es un administrador Global
      if (this.adminStationForm.valid) {
        let workStationElementToAdd = {
          boxNumber: 0,
          adress: '',
          switchEnable: false,
        };
        // new station
        //VER EN LINEA A CONTINUACION SI SE PUEDE PPREGUNAR POR EL SWITCH NO POR EL DESDE HASTA 
        if (this.adminStationForm.controls['firstBox'].value === '' && this.adminStationForm.controls['lastBox'].value === '') {
          // Only one workStation to add
          workStationElementToAdd.boxNumber = this.adminStationForm.controls['boxNumber'].value;
          workStationElementToAdd.adress = this.adminStationForm.controls['adress'].value;
          workStationElementToAdd.switchEnable = this.adminStationForm.controls['switchEnable'].value;

          this.adminService.createWorkStation(workStationElementToAdd).then(data => {
            if (data.status === 409) {
              this.msgModal = 'La estación que intentas agregar ya existe!'
            } else {
              this.getWorkStations(this.userCredentials);
              this.msgModal = 'Estación agregada con éxito!'
            }
            this.submit = false;
            this.cleanInputs();
            this.openModal(this.templateStationMessage);
          },
            err => {
              this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
              console.log(err)
            });
        } else {
          // Multiple workStations to add
          workStationsCountToAdd = parseInt(this.adminStationForm.controls['lastBox'].value);
          for (let i = this.adminStationForm.controls['firstBox'].value; i <= workStationsCountToAdd; i++) {
            this.adminStationForm.controls['boxNumber'].setValue(i);
            workStationElementToAdd.boxNumber = parseInt(i);
            workStationElementToAdd.adress = this.adminStationForm.controls['adress'].value;
            workStationElementToAdd.switchEnable = this.adminStationForm.controls['switchEnable'].value;


            this.adminService.createWorkStation(workStationElementToAdd).then(data => {

              if (data.status === 409) {
                this.msgModal = 'El grupo de estaciones que intentas agregar ya existe. Por favor ingresa un nuevo grupo.'
              } else {
                this.workStations.push(workStationElementToAdd);
                this.getWorkStations(this.userCredentials);
                this.msgModal = 'Estación/es agregadas con éxito! En caso de haber sobrescrito alguna estación, esta no será agregada.'
              }
            },
              err => {
                /* si imprimo un popup en caso de que sean multiples se tilda pq imprime mas de uno */
                /* this.openPopup('Error en la conexión. Por favor, intenta mas tarde.'); */
                console.log(err)
              });
          }
          this.submit = false;
          this.cleanInputs();
          this.openModal(this.templateStationMessage);
        }
      }
    }
    else {
      // Es un administrador Local
      if (this.adminStationForm.valid) {
        let workStationElementToAdd = {
          boxNumber: 0,
          adress: this.userCredentials.workStation.adress,
          switchEnable: false,
        };
        if (this.adminStationForm.controls['firstBox'].value === '' && this.adminStationForm.controls['lastBox'].value === '') {
          // Only one workStation to add
          workStationElementToAdd.boxNumber = this.adminStationForm.controls['boxNumber'].value;
          workStationElementToAdd.switchEnable = this.adminStationForm.controls['switchEnable'].value;
          this.adminService.createWorkStation(workStationElementToAdd).then(data => {
            if (data.status === 409) {
              this.msgModal = 'La estación que intentas agregar ya existe!'
            } else {
              this.getWorkStations(this.userCredentials);
              this.msgModal = 'Estación agregada con éxito!'
            }
            this.submit = false;
            this.cleanInputs();
            this.openModal(this.templateStationMessage);
          },
            err => {
              this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
              console.log(err)
            });
        } else {
          // Multiple workStations to add
          workStationsCountToAdd = parseInt(this.adminStationForm.controls['lastBox'].value);
          for (let i = this.adminStationForm.controls['firstBox'].value; i <= workStationsCountToAdd; i++) {
            this.adminStationForm.controls['boxNumber'].setValue(i);
            workStationElementToAdd.boxNumber = parseInt(i);
            workStationElementToAdd.switchEnable = this.adminStationForm.controls['switchEnable'].value;

            this.adminService.createWorkStation(workStationElementToAdd).then(data => {

              if (data.status === 409) {
                this.msgModal = 'El grupo de estaciones que intentas agregar ya existe. Por favor ingresa un nuevo grupo.'
              } else {
                this.workStations.push(workStationElementToAdd);
                this.getWorkStations(this.userCredentials);
                this.msgModal = 'Estación/es agregadas con éxito! En caso de haber sobrescrito alguna estación, esta no será agregada.'
              }
            },
              err => {
                this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
                console.log(err)
              });
          }
          this.submit = false;
          this.cleanInputs();
          this.openModal(this.templateStationMessage);
        }
      }
    }

  }

  updateWorkStationService() {
    this.submitEdit = true;

    const workstationEdit: Workstation = {
      adress: '',
      boxNumber: 0,
      switchEnable: false
    };


    if (this.adminStationFormEdit.valid) {
      workstationEdit.adress = this.adminStationFormEdit.controls['adressFormEdit'].value;
      workstationEdit.boxNumber = this.adminStationFormEdit.controls['boxNumberFormEdit'].value;
      workstationEdit.switchEnable = this.adminStationFormEdit.controls['switchEnableFormEdit'].value;
      this.adminService.updateWorkstation(this.idWorkStationToEdit, workstationEdit).subscribe(data => {
        this.msgModal = 'Estación modificada con éxito!';
        this.modalRef.hide()
        this.openModal(this.templateStationMessage);
        this.getWorkStations(this.userCredentials);
        this.submitEdit = false;
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    }
  }

  getWorkStations(userCredentials) {
    if (this.isGlobalAdmin) {
      // Obtengo todas
      this.adminService.getWorkStations().subscribe(data => {
        this.workStations = data.workStations;
        this.workStationsWithOutDuplicates = data.workStations
        this.copyWorkStation = data.workStations;
        this.workStationsWithOutDuplicates = this.removeDuplicates(data.workStations, 'adress');
        if (this.workStations.length === 0) {
          this.msgEstaciones = true
        } else {
          this.msgEstaciones = false
        }
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    } else {
      // Obtengo las que corresponden a la direccion del admin local
      this.adminService.getWorkStationsByAdress(userCredentials.workStation.adress).subscribe(data => {
        this.workStations = data.workStations;
        this.workStationsWithOutDuplicates = data.workStations
        this.copyWorkStation = data.workStations;
        if (this.workStations.length === 0) {
          this.msgEstaciones = true
        } else {
          this.msgEstaciones = false
        }
      },
        err => {
          this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
          console.log(err)
        });
    }

  }

  removeDuplicates(arr, comp) {
    const unique = arr
      .map(e => e[comp])
      .map((e, i, final) => final.indexOf(e) === i && i)
      .filter(e => arr[e]).map(e => arr[e]);
    return unique;
  }

  editWorkStation(idWorkStationToEdit) {
    this.idWorkStationToEdit = idWorkStationToEdit;
    this.buildFormEdit();
    this.openModal(this.templateEdit);
    this.adminService.getWorkStationById(idWorkStationToEdit).subscribe(data => {
      this.workStationToEdit = data.workStations;
      this.adminStationFormEdit.controls['adressFormEdit'].setValue(this.workStationToEdit.adress);
      this.adressToEdit = this.workStationToEdit.adress;
      this.adminStationFormEdit.controls['boxNumberFormEdit'].setValue(this.workStationToEdit.boxNumber);
      this.adminStationFormEdit.controls['switchEnableFormEdit'].setValue(this.workStationToEdit.switchEnable);
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });;
  }

  onWorkStationChange(workStationValue) {
    /* this.puestosEnUso = []
    this.workStations.map(workStation => {
      if (workStation.adress === workStationValue) {
        this.puestosEnUso.push(workStation.boxNumber)
      }
    }) */
  }

  searchStation(searchText) {
    if (searchText.value === '') {
      this.workStations = this.copyWorkStation;
    } else {
      this.workStations = this.copyWorkStation.filter(w => {
        return (w.adress.toLowerCase().includes(searchText.value.toLowerCase()) || w.boxNumber.toLowerCase().includes(searchText.value.toLowerCase()));
      })
    }
  }

  onMultipleBoxesChange() {
    /* SI SE SACAN LOS BUILD Y SE INGRESAN DOS OPCIONES SIMULTANEAS DE CAMPOS SE ROMPE TODO */
    this.multipleBoxes = !this.multipleBoxes;
    this.submit = false;

    if (this.isGlobalAdmin) {
      if (this.multipleBoxes && this.switchNewStation) {
        this.buildFormOneTwo();
      } else {
        if (this.multipleBoxes) {
          this.buildFormOneTwo();
        } else {
          if (this.switchNewStation) {
            this.buildFormOneOne();
          } else {
            this.buildFormOneOne();
          }
        }
      }
    } else {
      if (this.multipleBoxes) {
        this.buildFormLocalAdminMultiples()
      } else {
        this.buildFormLocalAdminOne()
      }
    }

    if (this.workStations.length === 0) {
      this.msgEstaciones = true
    } else {
      this.msgEstaciones = false
    }
  }

  onSwitchNewStationChange() {
    /* SI SE SACAN LOS BUILD Y SE INGRESAN DOS OPCIONES SIMULTANEAS DE CAMPOS SE ROMPE TODO */
    this.switchNewStation = !this.switchNewStation;
    this.submit = false;

    if (this.multipleBoxes && this.switchNewStation) {
      this.buildFormOneTwo();
    } else {
      if (this.multipleBoxes) {
        this.buildFormOneTwo();
      } else {
        if (this.switchNewStation) {
          this.buildFormOneOne();
        } else {
          this.buildFormOneOne();
        }
      }
    }
    if (this.workStations.length === 0) {
      this.msgEstaciones = true
    } else {
      this.msgEstaciones = false
    }
  }

  deleteWorkStation(idWorkStationToDelete) {
    this.adminService.deleteWorkStation(idWorkStationToDelete).subscribe(data => {
      this.getWorkStations(this.userCredentials);
    },
      err => {
        this.openPopup('Error en la conexión. Por favor, intenta mas tarde.');
        console.log(err)
      });
  }

  cleanInputs() {
    this.adminStationForm.controls.boxNumber.setValue('');
    this.adminStationForm.controls.firstBox.setValue('');
    this.adminStationForm.controls.lastBox.setValue('');
    this.adminStationForm.controls.adress.setValue('');

    if (this.workStations.length === 0) {
      this.msgEstaciones = true
    } else {
      this.msgEstaciones = false
    }
  }

  openPopup(text: string) {
    this.msgModal = text;
    this.modalRef = this.modalService.show(this.templateStationMessage);
  }

}


