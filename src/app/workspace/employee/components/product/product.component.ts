import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ProductService } from 'src/app/service/product.service';
import { Product } from 'src/app/interfaces/product.interface'
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { Turn } from 'src/app/interfaces/turn.interface';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnChanges {

  constructor(private productService: ProductService) { }

  @Input() turnSelected: Turn;
  @Output() turnProduct = new EventEmitter<Product[]>();
  productsList: Product[] = []
  filteredOptions: Observable<Product[]>;
  productSelected: Product[] = [];
  filteredProducts: Product[] = []
  autoCompleteInput = new FormControl();
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnChanges(changes) {
    if(changes.turnSelected.currentValue.product) {
      this.productSelected = changes.turnSelected.currentValue.products
    } else {
      this.productSelected = []
    }
  }

  ngOnInit() {
    console.log('entro');
    this.productService.getAllProduct().subscribe( data => {
      if(data.status === 200){
        this.productsList = data.products;
        if(this.turnSelected) {
          this.productSelected = this.turnSelected.products;
        } else {
          this.productSelected = []
        }

      }
    })
  }

  clearInput(event: MatChipInputEvent): void {
    const input = event.input;
    if (input) {
      input.value = '';
    }
  }

  addProduct(event: MatAutocompleteSelectedEvent) {
      const value = event.option.value;
      if ((value || '').trim()) {
        const product = this.productsList.find(x => x.name == value.trim())
        if (product) {
          this.productSelected.push(product);
        }
        this.turnProduct.emit(this.productSelected)
        this.autoCompleteInput.setValue('')
      }
  }


  removeProduct(product: any): void {
    const newList: Product[] = this.productSelected.filter( prod => prod !== product );
    this.productSelected = newList
    this.turnProduct.emit(this.productSelected)
  }

  filterProduct(searchText) {
    if (searchText.value === '') {
    } else {
      this.filteredProducts = this.productsList.filter(p => {
        return (p.name.toLowerCase().includes(searchText.value.toLowerCase()) || p.description.toLowerCase().includes(searchText.value.toLowerCase()))
      })
    }
  }

}
