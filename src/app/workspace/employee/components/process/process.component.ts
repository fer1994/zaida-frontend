import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Turn } from 'src/app/interfaces/turn.interface';
import { TurnService } from 'src/app/service/turn.service';
import { Process } from 'src/app/interfaces/process.interface';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss']
})
export class ProcessComponent implements OnInit, OnChanges {

  @Input() turnSelected: Turn;
  @Input() seconds: number;
  @Output() processSelected = new EventEmitter<Process>();
  processList: Process[] = [];
  process = new FormControl('undefined', );

  constructor(
    private turnService: TurnService,
  ) {}

  async ngOnInit() {
    await this.getProcess();
    if(this.turnSelected && this.turnSelected.process) {
      this.process.setValue(this.turnSelected.process._id);
    }
  }

  ngOnChanges( changes ) {
      if (changes.turnSelected && changes.turnSelected.currentValue.process) {
        this.process.setValue(changes.turnSelected.currentValue.process._id);
      }
      if (changes.turnSelected && (changes.turnSelected.currentValue.process === null ||
        !changes.turnSelected.currentValue.process)) {
        this.process.setValue('undefined');
      }
  }

  async getProcess() {
    await this.turnService.getAllProcess().toPromise().then( process => {
      this.processList = process.process;
    });
  }

  onChangeProcess(idProcess) {
    const process = this.processList.find( process => process._id === idProcess);
    this.processSelected.emit(process);
  }

}
