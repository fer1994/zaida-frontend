import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Turn } from 'src/app/interfaces/turn.interface';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnChanges {

  @Input() turnSelected: Turn;
  @Output() notes = new EventEmitter<string>();
  notesForm = new FormControl('');
  constructor() { }

  ngOnChanges( changes ) {
    if(changes.turnSelected && changes.turnSelected.currentValue.notes){
      this.notesForm.setValue(changes.turnSelected.currentValue.notes)
    }else {
      this.notesForm.setValue('')
    }
  }

  refreshNotes(event) {
    this.notes.emit(event.srcElement.value);
  }

}
