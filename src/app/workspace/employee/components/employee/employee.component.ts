import { Component, OnInit } from '@angular/core';
import { Turn } from 'src/app/interfaces/turn.interface';
import { Product } from '../../../../interfaces/product.interface';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  turnSelected: Turn = undefined;
  interval;
  seconds: number = 0;

  constructor() { }

  ngOnInit() {
   this.turnSelected = {
      code: 'IL52',
      group: 'Bv Illia 255',
      notes: [''],
      priority: 1,
   }
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.seconds++;
    }, 1000);
  }

  stopInterval() {
    clearInterval(this.interval);
    this.seconds = 0;
  }

  refheshTurnSelected(newTurnToRefresh: Turn) {
    this.turnSelected.time = this.turnSelected ? this.seconds : 0
    this.stopInterval();

    if (newTurnToRefresh) {
      this.seconds = newTurnToRefresh.time ? newTurnToRefresh.time : 0
      this.startTimer();
      this.turnSelected = newTurnToRefresh
    } else {
      this.turnSelected = {
        code: 'IL52',
        notes: [''],
        priority: 1,
        process: null
     }
      this.stopInterval();
      this.seconds = 0;
    }
  }

  refreshClientInfo(refreshClientInfo) {
    this.turnSelected.client = refreshClientInfo;
  }

  refreshNotes(notes) {
    if (notes) {
      this.turnSelected.notes = [notes]
    }
  }

  refreshProcess(process){
    if(process) {
      this.turnSelected.process = process;
    }
  }

  selectProducts(productList: Product[]) {
    if(productList) {
      this.turnSelected.products = productList;
    }
  }

}
