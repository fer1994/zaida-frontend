import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { TurnService } from 'src/app/service/turn.service';
import { Employee } from 'src/app/interfaces/employees.interface';
import { Turn } from 'src/app/interfaces/turn.interface';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-attention',
  templateUrl: './attention.component.html',
  styleUrls: ['./attention.component.scss']
})
export class AttentionComponent implements OnInit {

  @Output() turnSelectedOutput = new EventEmitter<Turn>();
  @Input() turnSelectedInput: Turn;
  station: string;
  userLogged: Employee = {};
  turnList: Turn[] = [];
  modalRef: BsModalRef;
  @ViewChild('templateFinish', { static: true }) modalTemplateFinish;
  @ViewChild('templateAcciones', { static: true }) modalTemplateAccion;
  motivoSeleted = '';
  motivoString = '';

  constructor(
    private turnService: TurnService,
    private modalService: BsModalService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.station = localStorage.getItem('station');
    this.userLogged = JSON.parse(localStorage.getItem('user'));
    this.getEmployeeTurn();
  }

  pauseTurn() {
    this.turnService.updateTurn(this.turnSelectedInput).subscribe( response => {});
    this.turnSelectedOutput.emit(null);
  }

  async getTurn() {
    await this.getNextTurnService().then( turn => {
      console.log('turn', turn);
      if (turn.status === 204) {
        // no existen turnos pendientes, creo uno vacío y se inicia
        const turnToCreate: Turn = {
          employee: this.userLogged,
          client: {
            name: 'Anonimo',
            lastName: '',
          },
          group: this.station,
          state: 'opened',
          time: 0,
        };
        this.createTurn(turnToCreate);
      } else {
        // existen turnos pendientes
        if (!turn.turn.client) {
          turn.turn.client = {
            name: 'Anonimo',
            lastName: '',
          };
        }

        if (!turn.turn.employee) {
          turn.turn.employee = this.userLogged;
        }

        this.turnList.push(turn.turn);
        this.selectClient(turn.turn);
      }

    }).catch( err => {
      console.log('err', err);
    });
  }


  async getNextTurnService(): Promise<any> {
     return await this.turnService.getNextTurn(this.userLogged._id, this.station);
  }

  getEmployeeTurn() {
    this.turnService.getOpenedTurns(this.userLogged._id).subscribe(data => {
      if (data.status === 200) {
        this.turnList = data.turn;
      }
      console.log( data.turn);
    }, error => {
      console.log(error);
    });
  }

  selectClient(turn) {
    if (this.turnSelectedInput.employee) {
      this.turnService.updateTurn(this.turnSelectedInput).subscribe( response => {});
    }
    this.turnSelectedOutput.emit(turn);
  }

  finishTurn(pendingAction: boolean, pendingActionString = '' ) {
    this.modalRef.hide();
    const turn = this.turnSelectedInput;
    this.turnSelectedInput.state = 'closed';

    if (pendingAction) {
      this.turnSelectedInput.pendingActions = pendingActionString;
    }

    this.turnService.updateTurn(this.turnSelectedInput).subscribe(data => {
      const turnoListAux = this.turnList.filter(ele => {
        return ele !== turn;
      });
      this.turnList = turnoListAux;
      this.turnSelectedOutput.emit(undefined);

      this._snackBar.open('Turno finalizado correctamente', 'Cerrar', {
        duration: 2000,
      });
    },
      error => {
        console.log(error);
        this._snackBar.open('Error al finalizar el turno', 'Cerrar', {
          duration: 2000,
        });
      });
    this.motivoSeleted = '';
    this.motivoString = '';
    this.modalRef.hide();

  }

  openPopupFinish(bandera: boolean) {
      if (!this.turnSelectedInput.process) {
        this._snackBar.open('Seleccione un proceso antes de continuar.', 'Cerrar', {
          duration: 5000,
        });
        return;
      }

      this.modalRef = this.modalService.show(this.modalTemplateFinish);
  }

  enviarAccion(pendingAction) {
    this.modalRef.hide();
    if (null !== pendingAction && pendingAction !== '') {
      this.finishTurn(true, this.motivoSeleted);
    } else {
      this._snackBar.open('Debe escribir una accion para continuar', 'Cerrar', {
        duration: 2000,
      });
    }

  }

  cloneTurn() {
    const newTurn = this.turnSelectedInput;
    newTurn._id = undefined;
    newTurn.time = 0;
    newTurn.group = this.station;
    newTurn.state = 'opened';

    if(!newTurn.client) {
      newTurn.client = {
        name: '',
        lastName: '',
      }
    }

    this.createTurn(newTurn);
  }

  createTurn(turn: Turn) {
    this.turnService.createTurn(turn).subscribe( data => {
      console.log('se creo correctamente', data);
      if ( data.status === 201) {
        this.turnList.push(data.turn);
        this.turnSelectedOutput.emit(data.turn);
      } else {
        console.log('Error');
      }
    });
  }

  freeTurn() {
      const turn = this.turnSelectedInput;
      turn.employee = null;
      turn.state = null;
      this.turnService.updateTurn(turn).subscribe(data => {
        const turnoListAux = this.turnList.filter(ele => {
          return ele !== turn;
        });
        this.turnList = turnoListAux;
        this.turnSelectedOutput.emit(undefined)
      },
      error => {
        console.log(error);
      });
  }

  closePopup() {
    this.modalRef.hide();
  }

  changeMotivo( motivo ) {
    if (motivo === 'Cliente no cumple requisitos'){
      this.motivoString = 'Especificar que requisito no cumplia';
    } else if ( motivo === 'Producto sin stock') {
      this.motivoString = 'Especificar si es equipo o accesorio y el modelo';
    } else if ( motivo === 'Problema durante la atención') {
      this.motivoString = 'Especificar el problema';
    } else {
      this.motivoString = '';
    }
  }

  openPopAcciones() {
    this.modalRef.hide();
    this.modalRef = this.modalService.show(this.modalTemplateAccion);
  }
}
