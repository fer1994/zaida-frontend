import { Component, Input, OnChanges } from '@angular/core';
import { Process } from 'src/app/interfaces/process.interface';
import { TurnService } from 'src/app/service/turn.service';

@Component({
  selector: 'app-related',
  templateUrl: './related.component.html',
  styleUrls: ['./related.component.scss']
})
export class RelatedComponent implements OnChanges {

  @Input() processSelected;
  processList: Process[] = [];

  constructor(
    private turnService: TurnService,
  ) { }

  ngOnChanges(changes) {
    this.processList = [];
    if(changes.processSelected.currentValue) {
      this.obtaingProcessSuggest(changes.processSelected.currentValue)
    }
    
  }

  addProceesRelated(processSelected){
   this.processList.forEach( (process) => {
     if ( process === processSelected) {
       process.isSelected = !process.isSelected;
     }
   })
 }

  obtaingProcessSuggest(process: Process) {
    if (process.suggestions && process.suggestions[0] !== "") {
      for (const idProcess of process.suggestions) {
        this.turnService.getProcessId(idProcess).subscribe(data => {
          this.processList.push(data.process);
        },
          error => {
            console.log('Error', error);
          });
      }
    }
  }

}
