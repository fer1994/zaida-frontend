import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Client } from 'src/app/interfaces/client.interface';
import { Turn } from 'src/app/interfaces/turn.interface';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit, OnChanges {

  @Input() turnSelected: Turn;
  @Output() clientInfo = new EventEmitter<any>();
  client: Client = {};
  informationForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes)
    if(changes.turnSelected && changes.turnSelected.currentValue.client){
      this.informationForm = this.fb.group({
        name: [changes.turnSelected.currentValue.client.name],
        lastName: [changes.turnSelected.currentValue.client.lastName],
        phone: [changes.turnSelected.currentValue.client.phone],
        dni: [changes.turnSelected.currentValue.client.dni],
        email: [changes.turnSelected.currentValue.client.email],
        origin: [changes.turnSelected.currentValue.client.origin]
      });

      if(!changes.turnSelected.currentValue.client.origin ||
          changes.turnSelected.currentValue.client.origin === '') {
        this.informationForm.controls.origin.setValue('Walkin')
      }

    } else {
      this.buildForm();
    }
  }

  get f1() { return this.informationForm.controls; }

  buildForm() {
    this.informationForm = this.fb.group({
      name: [''],
      lastName: [''],
      phone: [''],
      dni: [''],
      email: [''],
      origin: ['Walkin']
    });
  }

  refreshForm() {
    this.client = {
      name: this.informationForm.controls.name.value,
      lastName: this.informationForm.controls.lastName.value,
      phone: this.informationForm.controls.phone.value,
      dni: this.informationForm.controls.dni.value,
      email: this.informationForm.controls.email.value,
      origin: this.informationForm.controls.origin.value
    }
    this.clientInfo.emit(this.client)
  }

  changeOrigin() {
    this.refreshForm();
  }
}
