import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './components/employee/employee.component';
import { ProcessComponent } from './components/process/process.component';
import { MatFormFieldModule, MatInputModule, MatTooltipModule, MatAutocompleteModule, MatSnackBarModule, MatSelectModule } from '@angular/material';
import { ProductComponent } from './components/product/product.component';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { AttentionComponent } from './components/attention/attention.component';
import { InformationComponent } from './components/information/information.component';
import { RelatedComponent } from './components/related/related.component';
import { NotesComponent } from './components/notes/notes.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    EmployeeComponent,
    ProcessComponent,
    ProductComponent,
    AttentionComponent,
    InformationComponent,
    RelatedComponent,
    NotesComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatSnackBarModule,
    MatSelectModule
  ]
})
export class EmployeeModule { }
