import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from './service/settings.service';
import { SocketService } from './service/socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Zaida';
  fechaHora: Date = new Date();
  backendName: string = null;
  stationName: string = null;
  connected: boolean = false;
  username: string = 'Anonimo';
  userLogged: boolean;
  clients: any = [];

  constructor(
    private router: Router,
    private settingsService: SettingsService,
    private socketService: SocketService,
  ) { }

  startDate() {
    setInterval(() => {
      this.fechaHora = new Date();
    }, 1000);
  }

  ngOnInit() {
    this.startDate();
    this.isLogged();
    this.settingsService.station.subscribe(station => this.stationName = station);
    this.settingsService.connected.subscribe(connected => this.connected = connected);
    //this.settingsService.user.subscribe(user => this.username = user.email.split("@")[0]);
    this.getSettings();
    this.socketService.updateClients().subscribe(data => this.clients = data);
  }

  getSettings() {
    this.settingsService.getSettings()
  }

  isLogged() {
    const user =  localStorage.getItem('user');
    if (user === null) {
      this.router.navigate(['/login']);
      this.userLogged = false;
    } else {
      this.userLogged = true;
    }
  }

}


