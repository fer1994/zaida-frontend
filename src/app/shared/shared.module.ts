import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatHoursPipe } from './pipe/format-hours.pipe';

@NgModule({
  declarations: [FormatHoursPipe],
  imports: [
    CommonModule
  ],
  exports: [
    FormatHoursPipe
  ]
})
export class SharedModule { }
