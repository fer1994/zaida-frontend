import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatHours'
})
export class FormatHoursPipe implements PipeTransform {

  transform(value: number, args?: any): string {

    const hours: number = Math.floor(value / 60);
    const minutes: number = (value - hours * 60);

    if (hours > 0) {
        return hours + 'm' + ' ' + minutes + 's';
    } else {
        return minutes + 's';
    }
}

}
