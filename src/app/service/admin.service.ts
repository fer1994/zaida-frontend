import { Injectable } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpApiService: HttpApiService) {
  }

  private baseUrl: string = environment.url;

  createStaffMember(body: any) {
    return this.httpApiService.post(this.baseUrl + '/api/employees', body);
  }

  getStaffMemberById(id: any) {
    return this.httpApiService.get(this.baseUrl + '/api/employees/' + id);
  }

  updateStaffMember(idToEdit: any, body: any) : Observable<any> {
    return this.httpApiService.put(this.baseUrl + '/api/employees/' + idToEdit, body );
  }

  getStaffMembersByUserCredentials(userAdress: any) {
    return this.httpApiService.get(this.baseUrl + '/api/employees/search/' + userAdress);
  }

  deleteStaffMember(idStaffMemberToDelete: any) {
    return this.httpApiService.delete(this.baseUrl + '/api/employees/' + idStaffMemberToDelete);
  }

  getStaffMembers() {
    return this.httpApiService.get(this.baseUrl + '/api/employees');
  }

  async createWorkStation(body: any) {
    return await this.httpApiService.post(this.baseUrl + '/api/workStations', body).toPromise();
  }

  getWorkStationById(id: any) {
    return this.httpApiService.get(this.baseUrl + '/api/workStations/' + id);
  }

  getWorkStationsByAdress(adress: any) {
    return this.httpApiService.get(this.baseUrl + '/api/workStations/search/' + adress);
  }

  getWorkStationsByUserCredentials(userAdress: any) {
    return this.httpApiService.get(this.baseUrl + '/api/workStations/search/' + userAdress);
  }

  getWorkStations() {
    return this.httpApiService.get(this.baseUrl + '/api/workStations');
  }

  updateWorkstation(idToEdit: any, body: any) {
    return this.httpApiService.put(this.baseUrl + '/api/workStations/' + idToEdit, body );
  }

  deleteWorkStation(idWorkStationToDelete: any) {
    return this.httpApiService.delete(this.baseUrl + '/api/workStations/' + idWorkStationToDelete);
  }

  getProcesses() {
    return this.httpApiService.get(this.baseUrl + '/api/processes');
  }

  getProcessById(id: any) {
    return this.httpApiService.get(this.baseUrl + '/api/processes/' + id);
  }

  updateProcess(idToEdit: any, body: any) {
    return this.httpApiService.put(this.baseUrl + '/api/processes/' + idToEdit, body );
  }

  createProcess(body: any) {
    return this.httpApiService.post(this.baseUrl + '/api/processes', body);
  }

  deleteProcess(idProcessToDelete: any) {
    return this.httpApiService.delete(this.baseUrl + '/api/processes/' + idProcessToDelete);
  }

  getProducts() {
    return this.httpApiService.get(this.baseUrl + '/api/products');
  }

  getProductById(id: any) {
    return this.httpApiService.get(this.baseUrl + '/api/products/' + id);
  }

  updateProduct(idToEdit: any, body: any) {
    return this.httpApiService.put(this.baseUrl + '/api/products/' + idToEdit, body );
  }

  createProduct(body: any) {
    console.log('lo que se manda en el body: ', body)
    return this.httpApiService.post(this.baseUrl + '/api/products', body);
  }

  deleteProduct(idProcessToDelete: any) {
    return this.httpApiService.delete(this.baseUrl + '/api/products/' + idProcessToDelete);
  }
}
