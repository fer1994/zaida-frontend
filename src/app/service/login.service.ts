import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpApiService: HttpApiService) { }
  @Output() logged: EventEmitter<any> = new EventEmitter();
  private baseUrl: string = environment.url;

  logout(body: any) {
    this.logged.emit(false);
    return this.httpApiService.post(this.baseUrl + '/api/employees/logout', body);
  }

  getUsuario() {
    return this.httpApiService.get(this.baseUrl + '/api/employees');
  }

  deleteCerrarSesion() {
    this.logged.emit(false);
  }

  addCerrarSesion() {
    this.logged.emit(true);
  }

  loginSuccessful(logged: boolean) {
    if (logged) {
      this.logged.emit(true);
      let _date = new Date()
      localStorage.setItem('loginTime', _date.toISOString());
    } else {
      this.logged.emit(false);
    }
  }

  getStaffByUsernameAndPass(username, pass) {
    let body = { username, pass };
    return this.httpApiService.post(this.baseUrl + '/api/employees/login', body);
  }

  createUsuario(body: any) {
    return this.httpApiService.post(this.baseUrl + '/api/employees', body);
  }
}
