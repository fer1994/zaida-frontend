import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpApiService {

  constructor(private http: HttpClient) { }

  get(url: string): Observable<any> {
    return this.http.get(url);
  }

  getWithParam(url: string, params: any): Observable<any> {
    return this.http.get(url,  { params: params })
  }

  post(url: string, entity: any): Observable<any> {

    return this.http.post<any>(url, entity).pipe();
  }

  put(url, body) {
    return this.http.put(url, body);
  }

  patch() {
  }

  delete(url) {
    return this.http.delete(url);
  }

}
