import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class SocketService {
	private clients: Observable <string>;
    private url: string = environment.url;
    private socket;

    constructor() {
        this.socket = io(this.url);
    }

    public sendNickname(username) {
    	this.socket.emit('set-nickname', username);
    }

    public sendEmail(email) {
        this.socket.emit('set-email', email);
    }

    public sendStatus(status) {
        this.socket.emit('set-status', status);
    }

    public sendStation(station) {
        this.socket.emit('set-station', station);
    }

    public sendMessage(message) {
        this.socket.emit('new-message', message);
    }

    public updateClients():Observable<string> {
        return this.clients=new Observable((observer)=>{
          this.socket.on('update-clients', (data)=>observer.next(data));
          this.socket.emit('broadcast-data');
        })
  }
}