import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private httpApiService: HttpApiService) { }
  @Output() station: EventEmitter<any> = new EventEmitter();
  @Output() connected: EventEmitter<any> = new EventEmitter();
  @Output() user: EventEmitter<any> = new EventEmitter();
  private baseUrl: string = environment.url;

  getSettings() {
  	this.connected.emit(false)
  	const station = localStorage.getItem('station');
  	this.station.emit(station)
    const connected = this.httpApiService.get(this.baseUrl + '/api/settings');
    this.connected.emit(connected)
    const user = JSON.parse(localStorage.getItem('user'));
    this.user.emit(user)
  }

  getAllStation() {
    return this.httpApiService.get( this.baseUrl + '/api/workStations/address/all');
  }

  setUserName(user: any) {
    this.user.emit(user);
  }

  setNewStation(station){
    localStorage.setItem('station', station);
    this.station.emit(station);
  }

  setConnectionStatus(status) {
    this.connected.emit(status);
  }
}
