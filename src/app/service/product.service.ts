import { Injectable } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpApiService: HttpApiService) { }
  private baseUrl: string = environment.url;

  getAllProduct() {
    return this.httpApiService.get( this.baseUrl + '/api/products');
  }
}
