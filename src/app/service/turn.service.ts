import { Injectable } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { Turn } from '../interfaces/turn.interface';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TurnService {

  constructor(private httpApiService: HttpApiService) { }

  private baseUrl: string = environment.url;

  getAllProcess() {
    return this.httpApiService.get(this.baseUrl + '/api/processes');
  }

  createTurn(body: Turn) {
    return this.httpApiService.post(this.baseUrl + '/api/turns', body);
  }

  getProcessId(id: string) {
    return this.httpApiService.get(this.baseUrl + '/api/processes/' + id);
  }

  getAllTurns() {
    return this.httpApiService.get(this.baseUrl + '/api/turns');
  }

  updateTurn(body: Turn) {
    return this.httpApiService.put(this.baseUrl + '/api/turns/' + body._id, body);
  }

  getOpenedTurns(id: string) {
    return this.httpApiService.get(this.baseUrl + '/api/turns/search/' + id);
  }

  getClosedTurns(id: string) {
    return this.httpApiService.getWithParam(this.baseUrl + '/api/turns/search/' + id, {state: "closed"});
  }

  getTurnById(id: string) {
    return this.httpApiService.get(this.baseUrl + '/api/turns/' + id);
  }

  async getNextTurn(id: string, group: string) {
    return await this.httpApiService.get(this.baseUrl + '/api/turns/search/attend/' + id + '/' + group).toPromise();
  }

}
