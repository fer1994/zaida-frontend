import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { ReportsComponent } from './components/reports/reports.component';
import { ConfigurationPanelComponent } from './components/configuration-panel/configuration-panel.component';
import { ActivityPanelComponent } from './components/activity-panel/activity-panel.component';
import { TurnComponent } from './components/turn/turn.component';
import { CreationTurnComponent } from './components/creation-turn/creation-turn.component';
import { EmployeeComponent } from './workspace/employee/components/employee/employee.component';

export const ROUTES: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'welcome', component: WelcomeComponent},
    {path: 'reports', component: ReportsComponent},
    {path: 'configuration-panel', component: ConfigurationPanelComponent},
    {path: 'activity-panel', component: ActivityPanelComponent},
    {path: 'turn', component: TurnComponent},
    {path: 'turn-creation/:fastTurn', component: CreationTurnComponent},
    {path: 'turn-creation/:idTurn', component: CreationTurnComponent},
    {path: 'turn-creation', component: CreationTurnComponent},
    {path: 'employee', component: EmployeeComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'login'}
];
